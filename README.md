# Sample Project

This is sample project you can develop by this step:

1. Open Terminal in VS Code and type this command to checkout reference libraries
   go mod tidy

2. Go to accessors and make your_file.go
3. At accessors you should make logic to access 3rd party for example Restheart, Database, Webservice, Some Api or etc.
4. Go to controllers and make your_file.go
5. At controllers you should make logic about work flow step for example get variable from input and call accessor after that you can decide using "if" or "else" follow your work flow
6. At main.go you should call your controllers using serviceName
7. Commit and push your code when it's completed
8. If you commit your code and push to server, please wait line message. When server build your project complete or fail, it'll send message to you
9. If you get message build success you can test your api immediately
10. But you get message build fail please go to ElasticLog to track error
